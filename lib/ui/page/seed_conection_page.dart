import 'dart:async';

import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controller.dart';
import 'package:flare_flutter/flare_controls.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'package:flutter/physics.dart';

class SeedConnection extends StatefulWidget {
  @override
  _SeedConnectionState createState() => _SeedConnectionState();
}

class _SeedConnectionState extends State<SeedConnection>
    with SingleTickerProviderStateMixin {
  Animation<Offset> animation;
  Alignment seedPositionOAlignment = Alignment.center;
  AnimationController controller;

  Offset _centerOffset;
  Offset _currentOffset;

  var _startTweenOffset;

  var _flareController = FlareControls();

  var _backgroundColor = Colors.black87;

  bool _longPressActive = false;

  bool _rotate = false;

  void startAnimation() {
    _startTweenOffset = Offset(_currentOffset.dx, _currentOffset.dy);

    animation = math.Random().nextBool()
        ? MaterialPointArcTween(begin: _startTweenOffset, end: _centerOffset)
            .animate(controller)
        : Tween<Offset>(begin: _startTweenOffset, end: _centerOffset)
            .animate(controller);

    controller.value = 0;
    controller.animateTo(1, curve: Curves.elasticOut);
  }

  @override
  void initState() {
    super.initState();

    controller = AnimationController(
        duration: Duration(milliseconds: 1000), vsync: this);
    controller.addListener(() {
      setState(() {
//        _currentOffset = Offset.lerp(_startTweenOffset, _centerOffset, controller.value);
        _currentOffset = animation.value;
      });
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_centerOffset == null) {
      final size = MediaQuery.of(context).size;
      final height = size.height;
      final width = size.width;

      _centerOffset = Offset(width / 2, height / 2);
      _currentOffset = _centerOffset;
    }
    return Scaffold(
//      appBar: AppBar(
//        shape: buildAppBarShape(),
//        elevation: 20,
//        backgroundColor: Colors.green,
//        centerTitle: true,
//        title: Text("Seed Connection"),
      backgroundColor: _backgroundColor,
//      ),
      body: Transform.translate(
        offset: _currentOffset,
        child: FractionalTranslation(
          translation: Offset(-0.5, -0.5),
          child: GestureDetector(
            onTap: () => setState(() {
              _rotate = !_rotate;
            }),
            onLongPressStart: (LongPressStartDetails details) {
              setState(() {
                _longPressActive = true;
              });
            },
            onLongPressEnd: (LongPressEndDetails details) => setState(() {
              _longPressActive = false;
            }),
            onPanUpdate: (DragUpdateDetails details) {
              setState(() {
                _currentOffset = details.globalPosition;
              });
            },
            onPanDown: (details) {
              controller.stop();
            },
            onPanEnd: (DragEndDetails details) {
              startAnimation();
            },
            child: AnimatedContainer(
              curve: Curves.elasticOut,
              width: _longPressActive
                  ? 300
                  : 150, // zelis da container bude sto manji, tj da flare
              // bude sto manji, da nema puno mjesta okolo
              // da ne mogu uhvatiti u prazno
              height: _longPressActive ? 300 : 150,
              duration: Duration(seconds: _longPressActive ? 6 : 1),

//              color: Colors.pink.withOpacity(0.3),
              child: FlareActor("animations/ytt seed.flr",
                  fit: BoxFit.scaleDown,
                  isPaused: true,
                  controller: _flareController,
                  shouldClip: false,
                  alignment: Alignment.center,
                  animation: "treegrow"),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildDraggableFeedback(Color color) {
    return ClipOval(
      child: Container(
        color: color,
        width: 70,
        height: 70,
      ),
    );
  }
}

RoundedRectangleBorder buildAppBarShape() {
  return RoundedRectangleBorder(
    borderRadius: BorderRadius.only(
      bottomLeft: Radius.circular(33),
      bottomRight: Radius.circular(33),
    ),
  );
}
