import 'package:base_app/model/counter_model.dart';
import 'package:provider/provider.dart';

List<SingleChildCloneableWidget> providers = [
  ...models,
];

List<SingleChildCloneableWidget> models = [
  ...CounterModel.provide,
];
